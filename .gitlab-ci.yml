variables:
  USED_AS_LATEST: "ubuntu20.04-cuda11.2-mpich"
  DOCKERHUB_REGISTRY: "docker.io/ompcluster"
  USE_CACHE: 1

stages:
  - gen
  - build
  - test
  - deploy

hpccm-gen:
  stage: gen
  image: python:3-slim
  before_script:
    - pip3 install hpccm
  script:
    - python3 hpccm-recipe.py
  artifacts:
    paths:
      - ./Dockerfiles
      - ./SingularityDefinitions

##### Build image template
.build-img: &build-img
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - if [ "$USE_CACHE" -eq "1" ]; then CACHE_OPT="--cache=true"; fi
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor $CACHE_OPT --push-retry 5 --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfiles/$DEFINITION_FILE --destination $CI_REGISTRY_IMAGE/$DEFINITION_FILE:$CI_COMMIT_SHORT_SHA
  dependencies:
    - hpccm-gen
  tags:
    - f1-alveo

##### Test image template
.test-img: &test-img
  stage: test
  image:
    name: $CI_REGISTRY_IMAGE/$DEFINITION_FILE:$CI_COMMIT_SHORT_SHA
  script:
    - mpirun -np 2 /opt/osu/libexec/osu-micro-benchmarks/mpi/one-sided/osu_get_latency

##### Push image templates
.push-img: &push-img
  stage: deploy
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [""]
  script:
    - crane auth login $CI_REGISTRY -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD
    - crane auth login docker.io -u hyviquel -p $CI_DOCKERHUB_KEY
    # Only main branch use the name "hpcbase" otherwise use "hpcbase-dev"
    - |
        if [ "$CI_COMMIT_REF_NAME" = "main" ]; then
          export IMG_NAME="hpcbase"
          export IMG_TAG=$DEFINITION_FILE
        else
          export IMG_NAME="hpcbase-dev"
          export BRANCH_NAME=$(echo $CI_COMMIT_REF_NAME | sed 's/\//_/')
          export IMG_TAG=$BRANCH_NAME-$DEFINITION_FILE
        fi
        crane copy $CI_REGISTRY_IMAGE/$DEFINITION_FILE:$CI_COMMIT_SHORT_SHA $DOCKERHUB_REGISTRY/$IMG_NAME:$IMG_TAG
        # only define image as latest the previously chosen configuration
        if [ "$DEFINITION_FILE" = "$USED_AS_LATEST" ] && [ "$CI_COMMIT_REF_NAME" = "main" ]; then
          crane copy $DOCKERHUB_REGISTRY/$IMG_NAME:$IMG_TAG $DOCKERHUB_REGISTRY/$IMG_NAME:latest
        fi

###### Jobs

## Main images used for most testing

build:ubuntu20.04-cuda11.2-mpich:
  variables:
    DEFINITION_FILE: ubuntu20.04-cuda11.2-mpich
  <<: *build-img

test:ubuntu20.04-cuda11.2-mpich:
  variables:
    DEFINITION_FILE: ubuntu20.04-cuda11.2-mpich
  <<: *test-img
  dependencies:
    - build:ubuntu20.04-cuda11.2-mpich

push:ubuntu20.04-cuda11.2-mpich:
  variables:
    DEFINITION_FILE: ubuntu20.04-cuda11.2-mpich
  <<: *push-img
  dependencies:
    - test:ubuntu20.04-cuda11.2-mpich

build:ubuntu20.04-cuda11.2-mpich-ofed5:
  variables:
    DEFINITION_FILE: ubuntu20.04-cuda11.2-mpich-ofed5
  <<: *build-img

test:ubuntu20.04-cuda11.2-mpich-ofed5:
  variables:
    DEFINITION_FILE: ubuntu20.04-cuda11.2-mpich-ofed5
  <<: *test-img
  dependencies:
    - build:ubuntu20.04-cuda11.2-mpich-ofed5

push:ubuntu20.04-cuda11.2-mpich-ofed5:
  variables:
    DEFINITION_FILE: ubuntu20.04-cuda11.2-mpich-ofed5
  <<: *push-img
  dependencies:
    - test:ubuntu20.04-cuda11.2-mpich-ofed5

build:ubuntu20.04-cuda11.2-mpich-legacy:
  variables:
    DEFINITION_FILE: ubuntu20.04-cuda11.2-mpich-legacy
  <<: *build-img

test:ubuntu20.04-cuda11.2-mpich-legacy:
  variables:
    DEFINITION_FILE: ubuntu20.04-cuda11.2-mpich-legacy
  <<: *test-img
  dependencies:
    - build:ubuntu20.04-cuda11.2-mpich-legacy

push:ubuntu20.04-cuda11.2-mpich-legacy:
  variables:
    DEFINITION_FILE: ubuntu20.04-cuda11.2-mpich-legacy
  <<: *push-img
  dependencies:
    - test:ubuntu20.04-cuda11.2-mpich-legacy

build:ubuntu20.04-cuda11.2-openmpi:
  variables:
    DEFINITION_FILE: ubuntu20.04-cuda11.2-openmpi
  <<: *build-img

test:ubuntu20.04-cuda11.2-openmpi:
  variables:
    DEFINITION_FILE: ubuntu20.04-cuda11.2-openmpi
  <<: *test-img
  dependencies:
    - build:ubuntu20.04-cuda11.2-openmpi

push:ubuntu20.04-cuda11.2-openmpi:
  variables:
    DEFINITION_FILE: ubuntu20.04-cuda11.2-openmpi
  <<: *push-img
  dependencies:
    - test:ubuntu20.04-cuda11.2-openmpi

build:ubuntu20.04-cuda11.2-openmpi-legacy:
  variables:
    DEFINITION_FILE: ubuntu20.04-cuda11.2-openmpi-legacy
  <<: *build-img

test:ubuntu20.04-cuda11.2-openmpi-legacy:
  variables:
    DEFINITION_FILE: ubuntu20.04-cuda11.2-openmpi-legacy
  <<: *test-img
  dependencies:
    - build:ubuntu20.04-cuda11.2-openmpi-legacy

push:ubuntu20.04-cuda11.2-openmpi-legacy:
  variables:
    DEFINITION_FILE: ubuntu20.04-cuda11.2-openmpi-legacy
  <<: *push-img
  dependencies:
    - test:ubuntu20.04-cuda11.2-openmpi-legacy

# Test for Kahuna
# build:ubuntu20.04-openmpi-legacy:
#   variables:
#     DEFINITION_FILE: ubuntu20.04-openmpi-legacy
#   <<: *build-img

# test:ubuntu20.04-openmpi-legacy:
#   variables:
#     DEFINITION_FILE: ubuntu20.04-openmpi-legacy
#   <<: *test-img
#   dependencies:
#     - build:ubuntu20.04-openmpi-legacy

# push:ubuntu20.04-openmpi-legacy:
#   variables:
#     DEFINITION_FILE: ubuntu20.04-openmpi-legacy
#   <<: *push-img
#   dependencies:
#     - test:ubuntu20.04-openmpi-legacy
