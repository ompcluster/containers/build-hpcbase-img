#!/usr/bin/env python

from __future__ import print_function
from functools import partial

import re
import argparse
import os

from collections import namedtuple
from distutils.version import LooseVersion, StrictVersion

import hpccm
import hpccm.config

from hpccm.building_blocks import *
from hpccm.primitives import *
from hpccm.templates.git import git


def upstream_package_repos():
    """Return the package repositories."""

    codename = ''
    codename_ver = ''

    if (hpccm.config.g_linux_version >= StrictVersion('20.0') and
        hpccm.config.g_linux_version < StrictVersion('21.0')):
        codename = 'focal'
        codename_ver = 'focal-12'
    elif (hpccm.config.g_linux_version >= StrictVersion('18.0') and
        hpccm.config.g_linux_version < StrictVersion('19.0')):
        codename = 'bionic'
        codename_ver = 'bionic-12'
    else: # pragma: no cover
        raise RuntimeError('Unsupported Ubuntu version')

    return [
        'deb http://apt.llvm.org/{0}/ llvm-toolchain-{1} main'.format(codename, codename_ver),
        'deb-src http://apt.llvm.org/{0}/ llvm-toolchain-{1} main'.format(codename, codename_ver)]


def generateRecipe(baseImage, mpiConf, ofedConf):
    hasCuda = 'cuda' in baseImage
    hasUCX = 'legacy' not in mpiConf.impl and 'mvapich' not in mpiConf.impl
    hasOpenMPI = 'openmpi' in mpiConf.impl
    hasMPICH = 'mpich' in mpiConf.impl
    allowVCI = hasMPICH and hasUCX

    Stage0 = hpccm.Stage()

    # Start "Recipe"
    Stage0 += baseimage(image=baseImage)

    Stage0 += gnu()

    # Packages available with the same name for both apt and yum
    Stage0 += packages(ospackages=['gzip', 'wget', 'git', 'perl', 'curl',
                                   'ninja-build', 'sudo', 'ctags', 'bc',
                                   'autoconf', 'automake'],
                       epel=True)

    Stage0 += python(python2=True, python3=True, devel=True)

    Stage0 += packages(
        apt=['pdsh', 'ca-certificates', 'pkg-config', 'libglib2.0-dev',
             'build-essential', 'libelf-dev', 'libffi-dev', 'libssl-dev',
             'python3-distutils', 'python3-psutil', 'gdb', 'gnupg',
             'libgraphviz-dev']
        )

    Stage0 += packages(apt_keys=['https://apt.llvm.org/llvm-snapshot.gpg.key'],
                       apt_repositories=upstream_package_repos(),
                       apt=['g++-multilib', 'clang-12', 'llvm-12'])

    Stage0 += cmake(eula=True, version='3.22.6')

    # Communication libraries
    if hasCuda:
        Stage0 += gdrcopy(ldconfig=True, version='2.1')
    Stage0 += knem(ldconfig=True)
    Stage0 += xpmem(ldconfig=True)

    # Mellanox OFED support
    Stage0 += mlnx_ofed(version=ofedConf[1])

    # PMI support
    Stage0 += slurm_pmi2(prefix="/usr/local/pmi", version='20.11.9')

    if hasCuda:
        # Fix CUDA env variables
        Stage0 += environment(variables={'PATH': '/usr/local/cuda/bin:$PATH',
                                         'LIBRARY_PATH': '/usr/lib/x86_64-linux-gnu/:/usr/local/cuda/lib:/usr/local/cuda/lib64:/usr/local/lib:$LIBRARY_PATH',
                                         'LD_LIBRARY_PATH': '/usr/lib/x86_64-linux-gnu/:/usr/local/cuda/lib:/usr/local/cuda/lib64:/usr/local/lib:$LD_LIBRARY_PATH',
                                         'CUDA_HOME': '/usr/local/cuda/'})

    # Only add UCX dependencies when needed
    if hasUCX:
        # UCX default - RDMA-core based OFED
        Stage0 += ucx(cuda=hasCuda,
                      with_verbs="/usr", with_rdmacm="/usr",
                      knem="/usr/local/knem", ldconfig=True, ofed=True,
                      xpmem="/usr/local/xpmem", version='1.12.0',
                      gdrcopy="/usr/local/gdrcopy" if hasCuda else "",
                      disable_static=True, enable_mt=True)

    # Allow OpenMPI to work with root
    if hasOpenMPI:
        Stage0 += environment(variables={'OMPI_ALLOW_RUN_AS_ROOT': 1,
                                         'OMPI_ALLOW_RUN_AS_ROOT_CONFIRM': 1})

    # Performance and compatibility tuning
    Stage0 += environment(variables={'CUDA_CACHE_DISABLE': '1',
                                     'MELLANOX_VISIBLE_DEVICES': 'all', # enroot
                                     'UCX_TLS': 'all'})
    if hasOpenMPI and hasUCX:
        Stage0 += environment(variables={'OMPI_MCA_pml': 'ucx'})

    Stage0 += mpiConf.buildblock(cuda=hasCuda, version=mpiConf.version,
                                 prefix='/usr/local/mpi',
                                 ldconfig=True, disable_static=True)

    configureOpts = ["--with-ch4-max-vcis=64",
                     "--enable-thread-cs=per-vci"] if allowVCI else []

    Stage0 += mpiConf.buildblock(cuda=hasCuda, version=mpiConf.version,
                                 prefix='/usr/local/mpi',
                                 ldconfig=True, disable_static=True,
                                 configure_opts=configureOpts)

    Stage0 += generic_autotools(prefix='/opt/osu',
                                url='http://mvapich.cse.ohio-state.edu/download/mvapich/osu-micro-benchmarks-5.6.3.tar.gz',
                                configure_opts=['CC=mpicc', 'CXX=mpicxx'],
                                enable_cuda=hasCuda,
                                with_cuda_libpath='/usr/local/cuda/lib64/',
                                with_cuda_include='/usr/local/cuda/include/')

    # Install Lapack
    Stage0 += generic_cmake(cmake_opts=['-DCMAKE_BUILD_TYPE=Release',
                                        '-DBUILD_SHARED_LIBS=ON',
                                        '-DBUILD_TESTING=ON',
                                        '-DCBLAS=ON',
                                        '-DLAPACKE=ON',
                                        '-DLAPACKE_WITH_TMG=ON'],
                            preconfigure=['export OMP_NUM_THREADS=1'],
                            directory='lapack-3.9.0',
                            prefix='/opt/lapack',
                            url='https://github.com/Reference-LAPACK/lapack/archive/v3.9.0.tar.gz')

    if hasCuda:
        Stage0 += nsight_systems(version='2021.3.2', cli=False)
        Stage0 += nsight_compute(version='2021.2.2')

        Stage0 += generic_build(build=['make MPI=1 MPI_HOME=/usr/local/mpi'],
                                install=['mkdir -p /opt/nccl-tests && cp -R ./* /opt/nccl-tests'],
                                repository='https://github.com/NVIDIA/nccl-tests.git')

    # Install VELOC
    Stage0 += pip(upgrade=True, packages=['wget', 'bs4'], pip='pip3')
    Stage0 += generic_build(install=['mkdir -p /opt/veloc',
                                     './auto-install.py /opt/veloc'],
                            url='https://github.com/ECP-VeloC/VELOC/archive/veloc-1.4.tar.gz',
                            directory='VELOC-veloc-1.4')
    Stage0 += environment(variables={'LD_LIBRARY_PATH': '/opt/veloc/lib/:/opt/veloc/lib64/:$LD_LIBRARY_PATH',
                                     'LIBRARY_PATH': '/opt/veloc/lib/:/opt/veloc/lib64/:$LIBRARY_PATH'})

    # Add tests folder to containers
    Stage0 += copy(src='./tests/', dest='/opt/tests/', _post=True)

    # Install TMPI and its dependencies
    Stage0 += packages(ospackages=['tmux', 'reptyr'])
    Stage0 += generic_build(install=['./install.sh'],
                            repository='https://github.com/Azrael3000/tmpi.git')

    # Entrypoint
    Stage0 += copy(src='./utils/entrypoint.sh', dest='/usr/local/bin/entrypoint.sh')
    Stage0 += runscript(commands=['/usr/local/bin/entrypoint.sh'])

    hpccm.config.set_container_format('docker')

    return str(Stage0)


# format definition file name
def formatDefName(bImage, mpiConf, ofedConf):
    # get CUDA version using regexp
    cudaRe = re.compile('nvidia/cuda:[0-9]+[.[0-9]+]*')
    cudaMatch = cudaRe.match(bImage)
    if cudaMatch:
        cudaVerStr = str(cudaMatch.group()).replace("nvidia/", "")
        cudaVerStr = cudaVerStr.replace(":", "")

        # remove patch number from CUDA version
        minorVerRe = re.compile('cuda[0-9]+.[0-9]+.[0-9]+')
        if minorVerRe.match(cudaVerStr):
            cudaVerStr = cudaVerStr[:-2]

        # remove cuda version from base image name
        bImage = bImage[cudaMatch.end():]
        bImage = bImage.replace("-devel-", "")

    # remove separator between base image name and version
    defFileName = bImage.replace("-", "")
    defFileName = defFileName.replace(":", "")

    # add formatted cuda version to name
    if cudaMatch:
        defFileName += '-' + cudaVerStr

    # add MPI implementation description
    defFileName += '-' + mpiConf.impl

    # add ofed info only for 5.x version
    if ofedConf[0] == '5':
        defFileName += '-ofed' + ofedConf[0]

    return defFileName


def main():
    print("Generating HPC Base image definitions...")

    # Linux distributions to use as base images
    baseImages = [
        'nvidia/cuda:11.2.2-devel-ubuntu20.04',
        'ubuntu:20.04'
    ]

    ofedConfigurations = [
        ('4', '4.9-0.1.7.0'),
        ('5', '5.4-3.1.0.0')
    ]

    # MPI implementations to use in hpcbase images
    MpiConf = namedtuple('MpiConf', 'impl, buildblock, version')

    mpiConfigurations = [
        MpiConf(impl='openmpi-legacy', version='4.0.6',
                buildblock=partial(openmpi,
                                   infiniband=True, ucx=False,
                                   with_pmi='/usr/local/pmi')),
        MpiConf(impl='openmpi', version='4.0.6',
                buildblock=partial(openmpi,
                                   infiniband=False, ucx=True,
                                   enable_mca_no_build='btl-uct',
                                   with_pmi='/usr/local/pmi')),
        MpiConf(impl='mpich-legacy', version='3.3.2',
                buildblock=partial(mpich)),
        MpiConf(impl='mpich34', version='3.4.2',
                buildblock=partial(mpich, with_ucx='/usr/local/ucx',
                                   with_device='ch4:ucx')),
        MpiConf(impl='mpich', version='4.0.2',
                buildblock=partial(mpich, with_ucx='/usr/local/ucx',
                                   with_device='ch4:ucx')),
        MpiConf(impl='mvapich2', version='2.3.4',
                buildblock=partial(mvapich2))
    ]

    outputFolder = "Dockerfiles"

    for baseImage in baseImages:
        for mpiConf in mpiConfigurations:
            for ofedConf in ofedConfigurations:
                defFileText = generateRecipe(baseImage, mpiConf, ofedConf)
                defFileName = formatDefName(baseImage, mpiConf, ofedConf)

                # Save image definition file
                with open(f"{outputFolder}/{defFileName}", "w") as text_file:
                        text_file.write(str(defFileText))


if __name__ == "__main__":
    main()
