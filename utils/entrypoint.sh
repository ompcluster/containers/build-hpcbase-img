#!/bin/bash
set -e

if [ -d /usr/local/cuda/ ]; then
  if [[ "$(find /usr /.singularity.d -name libcuda.so.1 2>/dev/null) " == " " || "$(ls /dev/nvidiactl 2>/dev/null) " == " " ]]; then
    echo
    echo "WARNING: The NVIDIA Driver was not detected.  GPU functionality will not be available."
    if [[ -d /.singularity.d ]]; then
      echo "Use 'singularity run --nv' to start this container; see"
      echo "https://sylabs.io/guides/3.5/user-guide/gpu.html"
    else
      echo "Use 'docker run --gpus all' to start this container; see"
      echo "https://github.com/NVIDIA/nvidia-docker/wiki/Installation-(Native-GPU-Support)"
    fi
    echo
  fi
fi

if [ ! -d /dev/infiniband ]; then
  echo "WARNING: No InfiniBand devices detected."
  echo "         Multi-node communication performance may be reduced."
  echo
fi

DETECTED_MOFED=$(cat /sys/module/mlx5_core/version 2>/dev/null || true)
if [ -n "${DETECTED_MOFED}" ]; then
  # mlx5_core is not always present
  DETECTED_MOFED=$(cat /sys/module/mlx4_core/version 2>/dev/null || true)
fi

SUPPORTED_MOFED="4.5-1.0.1.0"

if [ -n "${DETECTED_MOFED}" ]; then
  if [[ ${DETECTED_MOFED} = ${SUPPORTED_MOFED} ]]; then
    # do nothing, use default config
    echo "Detected MOFED ${SUPPORTED_MOFED}."
  else
    echo "WARNING: Detected MOFED driver ${DETECTED_MOFED}, but this container does not support it."
	  echo "         Use ${SUPPORTED_MOFED} as a fall-back version."
	  echo "         Use of RDMA for multi-node communication may be be unreliable."
	  sleep 2
  fi
else
  echo "NOTE: MOFED driver for multi-node communication was not detected."
  echo "      Multi-node communication performance may be reduced."
fi

if [ -d /usr/local/cuda/ ]; then
  DETECTED_NVPEERMEM=$(cat /sys/module/nv_peer_mem/version 2>/dev/null || true)
  if [[ "${DETECTED_MOFED} " != " " && "${DETECTED_NVPEERMEM} " == " " ]]; then
    echo
    echo "NOTE: MOFED driver was detected, but nv_peer_mem driver was not detected."
    echo "      Multi-node communication performance may be reduced."
  fi
fi

echo

if [[ $# -eq 0 ]]; then
  exec "/bin/bash"
else
  exec "$@"
fi
