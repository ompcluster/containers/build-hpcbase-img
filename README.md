# Building HPC Base Images

This repository is used to build HPC Base container images. Those *hpcbase*
images contains the basic drivers and libraries necessary to run MPI programs on
HPC clusters. It contains:

  * Driver Mellanox
  * Driver NVIDIA and CUDA
  * MPI implementation (OpenMPI, MPICH or MVAPICH2)
  * Nsight Systems
  * OSU benchmarks
  * Git, CMake, Ninja, etc.

The containers images are built automatically by CI/CD and are available on
the following Docker Hub repository:
https://hub.docker.com/r/ompcluster/hpcbase

## Manual building

First, you need to install
[HPC Container Maker (HPCCM)](https://github.com/NVIDIA/hpc-container-maker),
an open-source tool to generates container specifications files (like
Dockerfiles). An HPCCM script is a Python recipe) that can generate multiple
container specifications.

HPCCM can be installed from  Pypi or Conda using the following commands:
```
sudo pip install hpccm
```
or
```
conda install -c conda-forge hpccm
```

Then, just run the following command to generate the definitions:
```
python3 hppcm-recipe.py
```

The script provided in this repository generates both Docker and Singularity
definitions. However, the Docker images performs some extra configurations that
makes it compatible with our Deploy-Swarm script: the password-less SSH
connection is configured inside the container, while Singularity can be run
using the host configuration.

Finally, any of the generated images can be built using Docker and Singularity.
